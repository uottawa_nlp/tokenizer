# Use Python 3.6
import os
import re, nltk, operator
import string
import yaml, io, pickle
import itertools
from nltk.probability import FreqDist
from nltk.corpus import stopwords


def main():
    path = os.getcwd()
    dataset = os.path.join(path, "res", "microblog2011.txt")
    create_yaml(os.path.join(path, "results", "statistics.yml"))
    normalize_1(dataset)


def normalize_1(dataset):
    normalize_non_english_chars(dataset)
    normalize_urls(dataset)
    normalize_numbers(dataset)
    create_tokens(dataset)


def create_tokens(dataset):
    path = os.getcwd()
    file_to_write = write_file(os.path.join(path, "results", "microblog2011_tokenized.txt"))
    freq_file = write_file(os.path.join(path, "results", "tokens_no_punct.txt"))
    freq_file_with_punct = write_file(os.path.join(path, "results", "Tokens.txt"))
    freq_file_no_stopwords = write_file(os.path.join(path, "results", "tokens_no_stopwords.txt"))

    corpus = read_file(dataset)
    token_list = []
    punct_token_list = []


    tokens_created = map(lambda tokens: nltk.word_tokenize(tokens), corpus)

    for single_map in tokens_created:
        for token in list(single_map):
            file_to_write.write(token + " ]][[ ")
            punct_token_list.append(token.lower())
        file_to_write.write("\n")

    file_to_write.close()
    unique_words = set(punct_token_list)

    fdist = FreqDist(punct_token_list)
    sorted_x = sorted(fdist.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    freq_1_tokens = 0
    for key, value in sorted_x:
        freq_file_with_punct.write(key + " : " + str(value))
        freq_file_with_punct.write("\n")
        if value is 1:
            freq_1_tokens = freq_1_tokens + 1
    freq_file_with_punct.close()

    uniq = str(len(unique_words))
    tok = str(len(punct_token_list))

    ratio = uniq + "/" + tok
    data1 = dict(
        TOTAL_TOKENS=len(punct_token_list),
        TOTAL_TYPES=len(unique_words),
        TYPE_TOKEN_RATIO=ratio,
        TOKENS_FREQ_1 = freq_1_tokens
    )

    data = dict(
        WITH_PUNCT_AND_SYMBOLS=data1
    )

    write_yaml(data, os.path.join(path, "results", "statistics.yml"))

    tokens_created = map(lambda tokens: nltk.word_tokenize(tokens), corpus)
    newList = remove_extra_words(tokens_created)
    for single_map in newList:
        for token in list(single_map):
            token_list.append(token.lower().strip())

    unique_words = set(token_list)

    freq_1_tokens = 0
    fdist = FreqDist(token_list)
    sorted_x = sorted(fdist.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    for key, value in sorted_x:
        freq_file.write(key + " : " + str(value))
        freq_file.write("\n")
        if value is 1:
            freq_1_tokens = freq_1_tokens + 1
    freq_file.close()

    uniq = str(len(unique_words))
    tok = str(len(token_list))

    ratio = uniq + "/" + tok
    data1 = dict(
        TOTAL_TOKENS=len(token_list),
        TOTAL_TYPES=len(unique_words),
        TYPE_TOKEN_RATIO=ratio,
        TOKENS_FREQ_1=freq_1_tokens
    )
    data = dict(
        LEXICAL_DIVERSITY=data1
    )

    write_yaml(data, os.path.join(path, "results", "statistics.yml"))

    stopwords_set = set(stopwords.words('english'))
    social_media_stopwords = set(['u', 'v', '\'s', '\'m', 'n\'t', 'rt', '\'re'])
    stopwords_set = stopwords_set.union(social_media_stopwords)

    filtered_words = filter(lambda token: token not in stopwords_set, token_list)
    list_without_stop_words = list(filtered_words)

    fdist = FreqDist(list_without_stop_words)
    sorted_x = sorted(fdist.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    counterFor100 = 0
    for key, value in sorted_x:
        freq_file_no_stopwords.write(key + " : " + str(value))
        freq_file_no_stopwords.write("\n")
        counterFor100 = counterFor100 + 1
        if counterFor100 == 100:
            break
    freq_file_no_stopwords.close()

    tokenized_corpus = map(lambda text: nltk.word_tokenize(text), corpus)

    translator = str.maketrans('', '', string.punctuation)

    word_tokenized_corpus = []
    for tokenized_text in list(tokenized_corpus):
        word_tokenized_text = []
        for token in tokenized_text:
            token = token.translate(translator).strip().lower()
            if token and token not in stopwords_set:
                word_tokenized_text.append(token)
        word_tokenized_corpus.append(word_tokenized_text)



    consecutive_pairs = {}
    for word_tokenized_text in word_tokenized_corpus:
        for word_token_a, word_token_b in \
            zip(word_tokenized_text, word_tokenized_text[1:]):

            pair = "%s_%s" % (word_token_a, word_token_b)
            if pair not in consecutive_pairs:
                consecutive_pairs[pair] = 1
            else:
                consecutive_pairs[pair] = consecutive_pairs[pair] + 1

    sorted_consecutive_pairs = sorted(consecutive_pairs.items(),
                                     key = operator.itemgetter(1), reverse=True)
    top_sorted_consecutive_pairs = sorted_consecutive_pairs[0:100]

    with open(os.path.join(path, \
             'results','top_consecutive_pairs.txt'),'w') as f:
        for consecutive_pair in top_sorted_consecutive_pairs:
            f.write(consecutive_pair[0] + ' : ' + str(consecutive_pair[1]))
            f.write('\n')

    word_tokens = list(itertools.chain.from_iterable(word_tokenized_corpus))
    word_types = set(word_tokens)

    lexical_density = dict(
        TOTAL_TOKENS=len(word_tokens),
        TOTAL_TYPES=len(word_types),
        TYPE_TOKEN_RATIO=str(len(word_types)) + "/" + str(len(word_tokens))
    )
    data = dict(
        LEXICAL_DENSITY=lexical_density
    )

    write_yaml(data, os.path.join(path, "results", "statistics.yml"))



def remove_extra_words(token_list):
    path = os.getcwd()
    new_list = []
    list_of_punct_symbols = read_file(os.path.join(path, "res", "punct.txt"))
    for symbol in list_of_punct_symbols:
        if symbol.__contains__("\n"):
            symbol = symbol.replace("\n", "")
        new_list.append(symbol)
    filtered_words = map(lambda sentence: filter(lambda token: token not in new_list, sentence), token_list)
    return filtered_words


def normalize_numbers(dataset):
    lines_to_write = search_for_numbers(dataset)
    modify_file(dataset, lines_to_write)


def normalize_urls(dataset):
    lines_to_write = search_for_urls(dataset)
    modify_file(dataset, lines_to_write)


def normalize_non_english_chars(dataset):
    lines_to_write = search_for_nonenglish_chars(dataset)
    modify_file(dataset, lines_to_write)


def search_for_numbers(dataset):
    file_contents = read_file(dataset)
    number_list = []
    count = 0
    for line in file_contents:
        number = re.findall("(\d*/?(.?),?:?;?\d+)", line)

        if len(number) is not 0:
            for number_val in number:
                line = line.replace(number_val[0], "")
                count = count + 1
        number_list.append(line)
    data = dict(
        NUMERIC=count
    )
    write_yaml(data, os.path.join(os.getcwd(), "results", "statistics.yml"))
    return number_list


def search_for_nonenglish_chars(dataset):
    file_contents = read_file(dataset)
    non_english_chars = []
    return_list = []
    count = 0
    for line in file_contents:
        for ch in line:
            if ord(ch) > 127:
                non_english_chars.append(ch)
            if ord(ch) == 10 or ord(ch) == 32:
                if len(non_english_chars) is not 0:
                    str1 = ''.join(non_english_chars)
                    line = line.replace(str1, "")
                    count = count + 1
                    non_english_chars = []
        return_list.append(str(line))
    data = dict(
        FOREIGN_WORDS=count
    )
    write_yaml(data, os.path.join(os.getcwd(), "results", "statistics.yml"))
    return return_list


def search_for_urls(dataset):
    file_contents = read_file(dataset)
    urls = []
    count = 0
    for line in file_contents:
        val = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', line)
        if len(val) is not 0:
            for singleval in val:
                line = line.replace(singleval, "")
                count = count + 1
        urls.append(line)
    data = dict(
        URL=count
    )
    write_yaml(data, os.path.join(os.getcwd(), "results", "statistics.yml"))
    return urls


def read_file(filepath):
    file = open(filepath, "r")
    filecontent = file.readlines()
    file.close()
    return filecontent


def write_file(filepath):
    file = open(filepath, "w")
    return file


# Write statistics to yaml file
def write_yaml(data, name):
    with io.open(name, 'a', encoding='utf8') as outfile:
        yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)
    outfile.close()


def create_yaml(name):
    io.open(name, 'w+', encoding='utf8')


def modify_file(filename, data):
    i = open(filename, 'w')
    for line in data:
        i.write(line)
    i.close()  # Close input file


if __name__ == "__main__":
    main()
    os._exit(1)
