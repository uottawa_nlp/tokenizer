import shutil, yaml, io, pickle


def readfile(filepath):
    file = open(filepath, "r",errors='ignore')
    filecontent = file.readlines()
    file.close()
    return filecontent



def writefile(filepath):
    file = open(filepath, "w")
    return file

#move or rename a file from src to dst
def renamefile(src, dst):
    shutil.move(src, dst)


# Read YAML file
# def readYaml():
#     with open("C:/Users/jdoshi/PycharmProjects/Tokenizer/res/application.yml", 'r') as stream:
#         data_loaded = yaml.load(stream)
#         return data_loaded

#Write statistics to yaml file
def writeYaml(data,name):
    with io.open(name, 'a', encoding='utf8') as outfile:
        yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)
    outfile.close()

def createYaml(name):
    io.open(name, 'w+', encoding='utf8')


def modify_file(filename, data):
    # Create temporary file read/write
   # t = tempfile.NamedTemporaryFile(mode="r+")
    i = open(filename, 'w')
    for line in data:
        i.write(line)
    # Copy input file to temporary file, modifying as we go

    # for line in i:
    #     #hasval = False
    #     for val in oldString:
    #         if not isinstance(val, (list, tuple)):
    #             val = [val]
    #         if line.__contains__(val[0]):
    #            # hasval = True
    #             line = line.replace(val[0],newString)
    #     # if hasval is False:
    #     #     t.write(line)
    #     #     break
    #     t.write(line)
    i.close()  # Close input file
    #t.seek(0)  # Rewind temporary file to beginning
    #o = open(filename, "w")
    # for line in t:
    #     o.write(line)
    # t.close()