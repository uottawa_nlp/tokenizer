import re, fileutil, nltk, operator
from nltk.probability import FreqDist
from nltk.corpus import  stopwords
import  os


def normalize(dataset):

   # normalizenonenglishchars(dataset)
    #normalizeurls(dataset)
    #normalizeNumbers(dataset)
    create_tokens(dataset)


def create_tokens(dataset):

    fileToWrite = fileutil.writefile('C:/Users/jdoshi/PycharmProjects/Tokenizer/results/microblog2011_tokenized.txt')
    freqFile = fileutil.writefile('C:/Users/jdoshi/PycharmProjects/Tokenizer/results/TokensNoPunct.txt')
    freqFileWithPunct = fileutil.writefile('C:/Users/jdoshi/PycharmProjects/Tokenizer/results/Tokens.txt')
    freqFileNoStopWords = fileutil.writefile('C:/Users/jdoshi/PycharmProjects/Tokenizer/results/TokensNoStopWords.txt')

    content = fileutil.readfile(dataset)
    tokenList = []
    punctTokenList = []

    tokensCreated = map(lambda tokens: nltk.word_tokenize(tokens), content)

    for singleMap in tokensCreated:
        for token in list(singleMap):
            punctTokenList.append(token.lower())

    unique_words = set(punctTokenList)

    for words in punctTokenList:
        fileToWrite.write(words + "\n")
    fileToWrite.close()

    fdist = FreqDist(punctTokenList)
    sorted_x = sorted(fdist.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    for key, value in sorted_x:
        freqFileWithPunct.write(key + " : " + str(value))
        freqFileWithPunct.write("\n")
    freqFileWithPunct.close()


    uniq = str(len(unique_words))
    tok = str(len(punctTokenList))

    ratio = uniq + "/" + tok
    data1 = dict(
        TOTAL_TOKENS=len(punctTokenList),
        UNIQUE_TOKENS=len(unique_words),
        TYPE_TOKEN_RATIO=ratio
    )

    data = dict(
        WITH_PUNCT_AND_SYMBOLS=data1
    )

    fileutil.writeYaml(data, 'C:/Users/jdoshi/PycharmProjects/Tokenizer/results/statistics.yml')

    tokensCreated = map(lambda tokens: nltk.word_tokenize(tokens), content)
    newList = removeExtraWords(tokensCreated)
    for singleMap in newList:
        for token in list(singleMap):
            tokenList.append(token.lower())

    unique_words = set(tokenList)

    fdist = FreqDist(tokenList)
    sorted_x = sorted(fdist.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    for key, value in sorted_x:
        freqFile.write(key + " : " + str(value))
        freqFile.write("\n")
    freqFile.close()

    uniq = str(len(unique_words))
    tok = str(len(tokenList))

    ratio = uniq + "/" + tok
    data1 = dict(
        TOTAL_TOKENS=len(tokenList),
        UNIQUE_TOKENS=len(unique_words),
        TYPE_TOKEN_RATIO=ratio
    )
    data = dict(
        LEXICAL_DIVERSITY=data1
    )
    fileutil.writeYaml(data, 'C:/Users/jdoshi/PycharmProjects/Tokenizer/results/statistics.yml')

    filtered_words = filter(lambda token: token not in stopwords.words('english'), tokenList)
    listWithoutStopWords = list(filtered_words)

    fdist = FreqDist(listWithoutStopWords)
    sorted_x = sorted(fdist.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    counterFor100 = 0
    for key, value in sorted_x:
        freqFileNoStopWords.write(key + " : " + str(value))
        freqFileNoStopWords.write("\n")
        counterFor100 = counterFor100 + 1
        if counterFor100 == 100:
            break
    freqFileNoStopWords.close()


def removeExtraWords(tokenList):
    newList = []
    listOfPunctSymbols = fileutil.readfile("C:\\Users\jdoshi\PycharmProjects\Tokenizer\\res\\punct.txt")
    for symbol in listOfPunctSymbols:
        if symbol.__contains__("\n"):
            symbol = symbol.replace("\n","")
        newList.append(symbol)
    filteredWords = map(lambda sentence: filter(lambda token: token not in newList,sentence), tokenList)
    return filteredWords

def normalizeNumbers(dataset):
    # replacedVal = fileutil.readYaml()
    # val = replacedVal["normalize"]["NUMERIC"]
    linesToWrite = searchForNumbers(dataset)
    fileutil.modify_file(dataset,linesToWrite)

def normalizeurls(dataset):
    # replacedval = fileutil.readYaml()
    # val = replacedval["normalize"]["URL"]
    linesToWrite = searchforurls(dataset)
    fileutil.modify_file(dataset, linesToWrite)

def normalizenonenglishchars(dataset):
    # replacedval = fileutil.readYaml()
    # val = replacedval["normalize"]["LATIN"]
    linesToWrite = searchfornonenglishchars(dataset)
    fileutil.modify_file(dataset, linesToWrite)

# def normalizeemojis(dataset):
#     emojis = searchforemojis(dataset)
#     replacedval = fileutil.readYaml()
#     val = replacedval["normalize"]["EMOJI"]
#     modify_file(dataset,emojis, val)
#     data = dict(
#         EMOJIS=len(emojis)
#     )
#     fileutil.writeYaml(data,'C:/Users/jdoshi/PycharmProjects/Tokenizer/results/statistics.yml')
#
# def searchforemojis(dataset):
#     filecontents = fileutil.readfile(dataset)
#     emojis = []
#     for line in filecontents:
#          if line.__contains__(emoji.get_emoji_regexp()):
#             c = emoji.emojize(line)
#             emojis.append(''.join(c))
#             #emojis.append(''.join(c for c in line if c in emoji.UNICODE_EMOJI))

def searchForNumbers(dataset):
    fileContents = fileutil.readfile(dataset)
    numberList = []
    count = 0
    for line in fileContents:
        #number = re.findall("[-+]?[.]?[\d]+(?:,/\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?", line)
        number = re.findall("(\d*/?(.?),?:?;?\d+)", line)

        if len(number) is not 0:
            for numberVal in number:
                line=line.replace(numberVal[0],"")
                count = count + 1
        numberList.append(line)
    data = dict(
        NUMERIC=count
    )
    fileutil.writeYaml(data, 'C:/Users/jdoshi/PycharmProjects/Tokenizer/results/statistics.yml')
    return numberList


def searchfornonenglishchars(dataset):
    filecontents = fileutil.readfile(dataset)
    nonenglishchars = []
    returlist = []
    count = 0
    for line in filecontents:
        for ch in line:
            if ord(ch) > 127:
                nonenglishchars.append(ch)
            if ord(ch) == 10 or ord(ch) == 32:
                if len(nonenglishchars) is not 0:
                    str1 = ''.join(nonenglishchars)
                    line = line.replace(str1,"")
                    count=count+1
                    nonenglishchars = []
        returlist.append(str(line))
    data = dict(
        LATIN=count
    )
    fileutil.writeYaml(data,'C:/Users/jdoshi/PycharmProjects/Tokenizer/results/statistics.yml')

    return returlist

def searchforurls(dataset):
    filecontents = fileutil.readfile(dataset)
    urls = []
    count = 0
    for line in filecontents:
        val = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', line)
        if len(val) is not 0:
            for singleval in val:
                line = line.replace(singleval,"")
                count = count + 1
        urls.append(line)
    data = dict(
        URL=count
    )
    fileutil.writeYaml(data, 'C:/Users/jdoshi/PycharmProjects/Tokenizer/results/statistics.yml')
    return urls


# def stemming(dataset):
#     listKeys, listValues = steminglemmatizing.stemming(dataset)
#     for i in range(len(listKeys)):
#         fileutil.modify_file(dataset,listKeys[i],listValues[i])

# def normalizepropernouns(dataset):
#     filecontents = fileutil.readfile(dataset)
#     lineCounter = 1
#     listofpropernouns = []
#     for line in filecontents:
#         propernouns = _propernouns_custom.extractProperNouns(line)
#         listofpropernouns.append(propernouns)
#         propernouns = ' '.join(propernouns)
#         if len(propernouns) is not 0:
#             data = dict(
#                 LINE=str(lineCounter),
#                 NOUN=propernouns
#             )
#             lineCounter = lineCounter + 1
#             fileutil.writeYaml(data,'C:/Users/jdoshi/PycharmProjects/Tokenizer/results/propernouns.yml')
