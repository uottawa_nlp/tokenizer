# Steps to execute the code in your local computer 

1. Ensure you have python 3.6
2. Run the commands present in `requirements.txt` file to install the necessary modules and packages, 
3. In the `res` directory, there would be a file called `microblog2011_orig.txt`, which is the master copy. The other file `microblog2011.txt` would become inconsistent after the execution of code. So, during the first time before executing the code and also before successive executions, copy the contents of `microblog2011_orig.txt` to a empty file `microblog2011.txt`
4. Run the following command to execute the code,
>
    python assignment_1_a_tokenizer.py
5. The required output files and few other output files would get generated in the `results` directory.
